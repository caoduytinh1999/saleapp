package com.example.doan.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.doan.CartActivity;
import com.example.doan.MainActivity;
import com.example.doan.R;
import com.example.doan.model.Cart;
import com.example.doan.model.DetailCart;
import com.example.doan.retrofit.APIUltils;
import com.example.doan.retrofit.DataClient;
import com.ramotion.foldingcell.FoldingCell;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LSMHAdapter extends RecyclerView.Adapter<LSMHAdapter.ItemHolder> {
    Context context;
    List<Cart> list;

    public LSMHAdapter(Context context, List<Cart> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lsmh,null);
        Animation ani = AnimationUtils.loadAnimation(context,R.anim.ani_itemlistview);
        view.setAnimation(ani);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {
        Cart cart = list.get(position);
        holder.txtSTT.setText(position + 1 + "");
        holder.txtNM.setText("Ngày Mua : " + cart.getNgayMua());
        holder.txtTThai.setText("Trạng Thái : " + cart.getTrangthai());
        DecimalFormat format = new DecimalFormat("###,###,###");
        holder.txtTT_title.setText("Tổng Tiền : " + format.format(Double.valueOf(cart.getTongtien())) + " VNĐ");
        holder.txtTT_content.setText("Tổng Tiền : " + format.format(Double.valueOf(cart.getTongtien()))+ " VNĐ");


        List<DetailCart> cartList = new ArrayList<>();
        ITCartAdapter adapter = new ITCartAdapter(holder.foldingCell.getContext(),cartList);
        GridLayoutManager layout = new GridLayoutManager(holder.foldingCell.getContext(),1);
       // holder.recyclerView.setHasFixedSize(false);
        holder.recyclerView.setFocusable(true);
        layout.setOrientation(GridLayoutManager.HORIZONTAL);
        holder.recyclerView.setLayoutManager(layout);
        holder.recyclerView.setAdapter(adapter);

        DataClient data = APIUltils.getData();
        Call<List<DetailCart>> callback = data.getDetailCart(cart.getId());
        callback.enqueue(new Callback<List<DetailCart>>() {
            @Override
            public void onResponse(Call<List<DetailCart>> call, Response<List<DetailCart>> response) {
                List<DetailCart> list1 = response.body();
                for (DetailCart item : list1){
                    cartList.add(new DetailCart(item.getId(),item.getIdGH(),item.getIdSP(),item.getHa(),item.getTen(),item.getSoluong(),item.getGia()));
                }
                Log.d("AAA",String.valueOf(cartList.size()));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<DetailCart>> call, Throwable t) {

            }
        });

        holder.foldingCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.foldingCell.toggle(false);
            }
        });

        holder.txtSTT.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                dialog.setTitle("XÁC NHẬN");
                dialog.setMessage("BẠN CÓ CHẮC CHẮN MUỐN HỦY ĐƠN HÀNG NÀY KHÔNG ?");
                dialog.setPositiveButton("CÓ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        holder.txtTThai.setText("Trạng Thái : HUY");
                        int id = cart.getId();
                        DataClient dataClient = APIUltils.getData();
                        Call<String> callback = dataClient.updateTT(id,"HUY");
                        callback.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                            }
                        });
                    }
                });

                dialog.setNegativeButton("KHÔNG", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                dialog.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ItemHolder extends RecyclerView.ViewHolder {

        FoldingCell foldingCell;
        TextView txtSTT,txtNM,txtTT_title,txtTT_content,txtTThai;
        CardView cardView;
        RecyclerView recyclerView;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);
            foldingCell = itemView.findViewById(R.id.fc);
            txtSTT = itemView.findViewById(R.id.txtSTT);
            txtNM = itemView.findViewById(R.id.txtNM_title);
            txtTT_title = itemView.findViewById(R.id.txtTT_LSMH);
            txtTT_content = itemView.findViewById(R.id.txtTT_Item_LSMH);
            recyclerView = itemView.findViewById(R.id.recyclerViewItemLSMH);
            txtTThai = itemView.findViewById(R.id.txtTThai);
            cardView = itemView.findViewById(R.id.cvItemCart_LSMH);
        }
    }
}
