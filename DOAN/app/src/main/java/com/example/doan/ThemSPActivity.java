package com.example.doan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.doan.model.SanPham;
import com.example.doan.retrofit.APIUltils;
import com.example.doan.retrofit.DataClient;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ThemSPActivity extends AppCompatActivity {

    EditText editTen,editGia,editHA,editMota,editKM;
    Spinner spinner;
    Button btnCapNhat;
    ImageView img;
    String[] loaisp = {"LAPTOP","DIEN THOAT"};
    public static boolean checkSPM = false;
    public static String tensp;
    public static String motasp;
    public static String giasp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_s_p);
        setControl();
        setEvent();
    }

    private void setEvent() {
        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,loaisp);
        spinner.setAdapter(adapter);

//        editHA.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                String link = editHA.getText().toString();
//                Picasso.with(ThemSPActivity.this).load(link).placeholder(R.drawable.folder).error(R.drawable.folder).into(img);
//            }
//        });

        editHA.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Toast.makeText(ThemSPActivity.this, "hihi", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String link = editHA.getText().toString();
                if (link.isEmpty()){
                    link = "link";
                }
                Picasso.with(ThemSPActivity.this).load(link).placeholder(R.drawable.folder).error(R.drawable.warning).into(img);
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(ThemSPActivity.this, "huhu", Toast.LENGTH_SHORT).show();
            }
        });

        btnCapNhat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ten = editTen.getText().toString();
                tensp = ten;
                String gia = editGia.getText().toString();
                giasp = gia;
                String mota = editMota.getText().toString();
                motasp = mota;
                String ha = editHA.getText().toString();
                String km = editKM.getText().toString();
                int idloaisp = 1;
                String loaisp = (String) spinner.getSelectedItem();
                if (!(loaisp.compareTo("LAPTOP") == 0)){
                    idloaisp = 2;
                }
                DataClient data = APIUltils.getData();
                Call<String> callback = data.insertSP(ten,idloaisp,gia,km,mota,ha);
                callback.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String result = response.body();
                        if (result.compareTo("success") ==0){
                            Toast.makeText(ThemSPActivity.this, "Thêm Sản Phẩm Thành Công", Toast.LENGTH_SHORT).show();
                            checkSPM = true;
                            startActivity(new Intent(ThemSPActivity.this,QLSPActivity.class));
                        }
                        else{
                            Toast.makeText(ThemSPActivity.this, "Thêm Sản Phẩm Thất Bại", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Toast.makeText(ThemSPActivity.this, "Lỗi", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


    }

    private void setControl() {
        editTen     = findViewById(R.id.editTenSP_Them);
        editGia     = findViewById(R.id.editGia_Them);
        editHA      = findViewById(R.id.editHA_Them);
        editMota    = findViewById(R.id.editMota_Them);
        editKM      = findViewById(R.id.editKM_Them);
        spinner     = findViewById(R.id.spinerThemSP);
        btnCapNhat  = findViewById(R.id.btnCapNhat_Them);
        img         = findViewById(R.id.imgHA_Them);
    }
}